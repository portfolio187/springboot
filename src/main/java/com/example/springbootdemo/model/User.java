package com.example.springbootdemo.model;

import java.util.Objects;
import java.util.UUID;

public class User {

    private String username;
    int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User(String username , int userID) {
        this.username = username;
        this.userId = userID;

    }

    public String getName() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
