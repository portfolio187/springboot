package com.example.springbootdemo.model;

public class UserRequestDTO {
    String username;
    int userId;

    public UserRequestDTO() {
    }

    public UserRequestDTO(String username, int userId) {
        this.username = username;
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public User toUser(){
        return new User(username , userId);
    }
}
