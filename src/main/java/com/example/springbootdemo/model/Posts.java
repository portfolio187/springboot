package com.example.springbootdemo.model;

import lombok.Data;

@Data
public class Posts {
    int userId;
    int id;
    String title;
    String body;


    public Posts() {
    }

    public Posts(int userId, int id, String title, String body) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.body = body;
    }
}
