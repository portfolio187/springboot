package com.example.springbootdemo.repository;

import com.example.springbootdemo.model.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository {
    List<User> users = new ArrayList<>(
            List.of()
    );

    public User findUserById(int id) {
        return users.stream().filter(user -> user.getUserId()
                == id).findFirst().orElseThrow();
    }

    public void deleteById(int id) {
        users.removeIf(user -> user.getUserId() == id);
    }

    public User saveUser(User user) {
        users.add(user);
        return users.get(users.indexOf(user));
    }

    public User updateUser(User user) {
        int index = users.indexOf(user);
        return users.set(index, user);
    }
}
