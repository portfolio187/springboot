package com.example.springbootdemo.service;

import com.example.springbootdemo.model.Posts;
import com.example.springbootdemo.model.User;
import com.example.springbootdemo.model.UserRequestDTO;
import com.example.springbootdemo.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    WebClient webClient;


    public User findUserById(int id) {
        return userRepository.findUserById(id);
    }

    public void deleteById(int id) {
      userRepository.deleteById(id);
    }

    public User createUser(UserRequestDTO userReguestDTO) {
        User user = userReguestDTO.toUser();
        return userRepository.saveUser(user);
    }

    public User updateUser(int id ,UserRequestDTO userRequestDTO) {
        User user = userRepository.findUserById(id);
        BeanUtils.copyProperties(userRequestDTO,user);
        return userRepository.updateUser(user);
    }

    public Posts[] getPostByUserId(int id) {
        return webClient.get()
                .uri("https://jsonplaceholder.typicode.com/posts/?userId=" + id)
                .exchangeToMono(clientResponse -> clientResponse.bodyToMono(Posts[].class))
                .block();

    }
}
