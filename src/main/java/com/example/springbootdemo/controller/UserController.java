package com.example.springbootdemo.controller;

import com.example.springbootdemo.model.Posts;
import com.example.springbootdemo.model.User;
import com.example.springbootdemo.model.UserRequestDTO;
import com.example.springbootdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(value = "http://localhost:5000")
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    UserService userService;


    @GetMapping("/{id}")
    public User findUserById(@PathVariable("id") int id){
        return userService.findUserById(id);

    }
    @PostMapping
    public User createUser(@RequestBody UserRequestDTO userReguestDTO){

        return userService.createUser(userReguestDTO);
    }

    @PutMapping("/{id}")
    public User updateUser(@PathVariable ("id") int id,@RequestBody UserRequestDTO userRequestDTO){
        return userService.updateUser(id, userRequestDTO);
    }
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") int id){
        userService.deleteById(id);
    }


    @GetMapping("/{id}/posts")
    public Posts[] getPostByUserId(@PathVariable("id") int id){
        return userService.getPostByUserId(id);
    }

    
}
